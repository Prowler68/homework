﻿using API_Customers.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Customers.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
                
        public static List<Customer> customers = new List<Customer>()
        {
            new Customer { FirstName = "Franta", LastName = "Primus", VipStat = true },
            new Customer { FirstName = "Pepa", LastName = "Pilus", VipStat = false }
        };

        
        public CustomersController() { }

        [HttpGet("getAll")]
        public ActionResult<IEnumerable<Customer>> GetAll()
        {
            return customers;
        }

        [HttpGet("get")]
        public ActionResult<Customer> GetCustomer(Guid id)
        {
            var customer = customers.FirstOrDefault(x => x.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            return customer;
        }

        [HttpPost("post")]
        public ActionResult<Customer> PostCustomer(string firstName, string lastName, bool vipStat)
        {
            Customer customer = new Customer { FirstName = firstName, LastName = lastName, VipStat = vipStat };
            customers.Add(customer);
            return customer;
        }

        [HttpDelete("delete")]
        public ActionResult<Customer> DeleteCustomer(Guid id)
        {
            var customer = customers.FirstOrDefault(x => x.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            customers.Remove(customer);
            return customer;
        }

        [HttpPut("put")]
        public ActionResult<Customer> EditCustomer(Guid id, string firstName, string lastName, bool vipStat)
        {
            var customer = customers.FirstOrDefault(x => x.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            customer.FirstName = firstName;
            customer.LastName = lastName;
            customer.VipStat = vipStat;
            return customer;
        }
    }
}
