﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Customers.Models
{
    public class Customer
    {
        public Guid Id { get; set;  }
        public DateTime RegTime { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool VipStat { get; set; }

        public Customer()
        {
            Id = Guid.NewGuid();
            RegTime = DateTime.Now;
        }
        
    }
}
