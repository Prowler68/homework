import React from 'react';
import 'antd/dist/antd.css';
import '../App.css';
import Axios from 'axios';
import { Typography, Layout, Input, Checkbox, Button, Table, Tag } from 'antd';

export default class Api extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            customerList: [],
            newFirstName: "",
            newLastName: "",
            newVipStat: false,
            editID: ""
        }

        this.add = this.add.bind(this);
        this.getAll = this.getAll.bind(this);
        this.post = this.post.bind(this);
        this.delete = this.delete.bind(this);
        this.edit = this.edit.bind(this);
        this.passFirstName = this.passFirstName.bind(this);
        this.passLastName = this.passLastName.bind(this);
        this.passVipStat = this.passVipStat.bind(this);
        this.passEdit = this.passEdit.bind(this);
    }

    //METODY ZAJIŠŤUJÍCÍ AKTUALIZACI LOKÁLNÍHO SEZNAMU ZÁKAZNÍKŮ
    async getAll() {
        let url = "https://localhost:5001";
        let endpoint = "/customers";
        let response = await Axios.get(url + endpoint + "/getall");
        console.log("GetAll:", response);
        return response.data;
    }
    async add() {
        let list = await this.getAll();
        this.setState({ customerList: list });
    }
    async componentDidMount() {
        let data = await this.getAll("info");
        this.setState({ customerList: data });
        console.log("CustomerList:", this.state.customerList)
    }

    //METODY PŘIDÁNÍ NOVÉHO ZÁPISU
    async post(firstName = this.state.newFirstName, lastName = this.state.newLastName, vipStat = this.state.newVipStat) {
        let url = "https://localhost:5001";
        let endpoint = "/customers";
        let response = await Axios.post(url + endpoint + "/post?firstName=" + firstName + "&lastName=" + lastName + "&vipStat=" + vipStat);
        this.componentDidMount();
        this.setState({ newFirstName: "" });
        this.setState({ newLastName: "" });
        this.setState({ newVipStat: false });
        console.log("NewCustomer:", response);
        return response.data;
    }
    passFirstName(args) {
        this.setState({ newFirstName: args.target.value })
    }
    passLastName(args) {
        this.setState({ newLastName: args.target.value })
    }
    passVipStat() {
        this.state.newVipStat === true ? this.setState({ newVipStat: false }) : this.setState({ newVipStat: true })
    }

    //METODY ODEBÍRÁNÍ STÁVAJÍCÍCH ZÁPISŮ
    async delete(id) {
        let url = "https://localhost:5001";
        let endpoint = "/customers";
        let response = await Axios.delete(url + endpoint + "/delete?id=" + id);
        this.componentDidMount();
        console.log("DeletedCustomer:", response);
        return response.data;
    }

    //METODY EDITACE STÁVAJÍCÍCH ZÁPISŮ
    async edit(id = this.state.editID, firstName = this.state.newFirstName, lastName = this.state.newLastName, vipStat = this.state.newVipStat) {
        let url = "https://localhost:5001";
        let endpoint = "/customers";
        let response = await Axios.put(url + endpoint + "/put?id=" + id + "&firstName=" + firstName + "&lastName=" + lastName + "&vipStat=" + vipStat);
        this.componentDidMount();
        this.setState({ newFirstName: "" });
        this.setState({ newLastName: "" });
        this.setState({ newVipStat: false });
        console.log("EditedCustomer:", response);
        return response.data;
    }
    passEdit(id) {
        this.setState({ editID: id });
        let data = this.state.customerList.find(a => a.id === id);
        console.log(data);
        this.setState({ newFirstName: data.firstName });
        this.setState({ newLastName: data.lastName });
        this.setState({ newVipStat: data.vipStat });
    }

    //SERIALIZACE FORMÁTU ČASU
    //"2021-06-18T00:14:58.2039135+02:00" -> "00:14:58 - 18.06.2021"
    serializeDateTime(dateTime) {
        let year = dateTime.slice(0, 4);
        let month = dateTime.slice(5, 7);
        let day = dateTime.slice(8, 10);
        let time = dateTime.slice(11, 19);

        let date = time + " - " + day + "." + month + "." + year
        console.log("timeComponents:", time, day, month, year, "serializedDate:", date);
        return date;
    }



    render() {
        const { Header, Sider, Content } = Layout;
        const { Title, Text } = Typography;
        const { Column, ColumnGroup } = Table;

        return (<div>
            <Layout>
                <Header>
                    <Title type="warning">Customers</Title>
                </Header>
                <Layout>
                    <Sider>
                        <Input placeholder="First Name" onChange={this.passFirstName} value={this.state.newFirstName}></Input>
                        <Input placeholder="Last Name" onChange={this.passLastName} value={this.state.newLastName}></Input>
                        <Checkbox onChange={this.passVipStat}></Checkbox>
                        <Text type="warning">VIP Status</Text>
                        <Button type="primary" onClick={a => this.post()} >Add New Customer</Button>
                        <Button type="default" onClick={a => this.edit()} >Save Changes</Button>
                    </Sider>
                    <Content>
                        <Table dataSource={this.state.customerList}>
                            <ColumnGroup title="Name">
                                <Column title="First Name" dataIndex="firstName" key="firstName" />
                                <Column title="Last Name" dataIndex="lastName" key="lastName" />
                            </ColumnGroup>
                            <Column
                                title="Registered"
                                dataIndex="regTime"
                                key="regTime"
                                render={(regTime) => this.serializeDateTime(regTime)}
                            />
                            <Column
                                title="VIP Status"
                                dataIndex="vipStat"
                                key="vipStat"
                                render={dataIndex => (
                                    <>
                                        {dataIndex === true ?
                                            <Tag color="blue">
                                                VIP
                                            </Tag> :
                                            <Tag color="red">
                                                Common
                                            </Tag>}
                                    </>
                                )}
                            />
                            <Column
                                title="Delete"
                                dataIndex="id"
                                key="id"
                                render={(id) => <Button type="link" onClick={a => this.delete(id)} >Delete</Button>}
                            />
                            <Column
                                title="Edit"
                                dataIndex="id"
                                key="id"
                                render={(id) => <Button type="link" onClick={a => this.passEdit(id)} >Edit</Button>}
                            />
                        </Table>
                    </Content>
                </Layout>
            </Layout>
        </div>)
    }
}